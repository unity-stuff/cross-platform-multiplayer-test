using Unity.Netcode;
using UnityEngine.SceneManagement;

public class TestBehavior : NetworkBehaviour
{
    private void Awake()
    {
        _relayExample = FindObjectOfType<RelayExample>();
    }

    private int _message = 0;
    private RelayExample _relayExample;
    private const string TestMessage = @"Test Message";
    public void SendMessage()
    {
        SendMessageServerRpc(TestMessage + _message++);
    }

    [ServerRpc(RequireOwnership = false)]
    private void SendMessageServerRpc(string testMessage)
    {
        SendMessageClientRpc(testMessage);
    }

    [ClientRpc]
    private void SendMessageClientRpc(string testMessage)
    {
        _relayExample.DisplayMessage(testMessage);
    }

    public void ChangeScene()
    {
        NetworkManager.Singleton.SceneManager.LoadScene("Scene Two", LoadSceneMode.Single);
    }
}
